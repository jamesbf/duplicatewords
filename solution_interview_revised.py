import re

# Complete the function below.
empty_array = [None for _ in xrange(ord('z')-ord('A')+1)]
ord_a = ord('A')

class Node(object):
    def __init__(self, c):
        self.children = list(empty_array)
        self.endPoint = False
        self.char = c

def get_word_iter(s):
    return (x.group(0) for x in re.finditer(r"[^\s\t\,\;\.\-]+", s))


def check_for_word(word, root_node):
    """Returns true if word has been found already"""
    curr_node = root_node

    for index, c in enumerate(word):
        found_node = curr_node.children[ord(c)-ord_a]
        if found_node:
            curr_node = found_node
            continue

        new_node = None
        for nc in word[index:]:
            new_node = Node(nc)
            curr_node.children[ord(nc)-ord_a] = new_node
            curr_node = new_node
        new_node.endPoint = True
        return False

    found = curr_node.endPoint
    curr_node.endPoint = True
    return found


def run(s):
    root_node = Node(None)
    for word in get_word_iter(s):
        if check_for_word(word, root_node):
            return word