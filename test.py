#!/bin/python
import time


class Enum(set):
    def __getattr__(self, name):
        if name in self:
            return name
        raise AttributeError

Result = Enum(["NotRun", "Failed", "Passed", "Aborted"])

class TestCase(object):
    def __init__(self, input, expected_output):
        self.input = input
        self.expected_output = expected_output


class TestResult(object):
    def __init__(self, method, duration, output, result, message):
        self.method = method
        self.duration = duration
        self.output = output
        self.result = result
        self.message = message

    def __str__(self):
        s = "Method: {0}\nOutput: {1}\nRun Time: {2}\nResult: {3}". format(
            self.method.__module__ + '.' + self.method.__name__,
            self.output,
            self.duration,
            str(self.result),
        )
        if self.message:
            s += "\nMessage: {}".format(self.message)
        return s


def time_it(s):
    start = time.time()
    for _ in range(10000):
        s()
    return time.time()-start


def run_tests(tests, methods):
    test_results = list()
    output = None
    duration = -1
    for test in tests:
        for method in methods:
            message = ''
            try:
                output = method(test.input)
                duration = time_it(lambda: method(test.input))
                result = Result.Passed if output == test.expected_output else Result.Failed
            except Exception as e:
                result = Result.Aborted
                message = str(e)

            test_results.append(TestResult(method, duration, output, result, message))

    return test_results

import solution_interview
import solution_interview_revised
import solution_dict

solutions = [
    solution_interview.run,
    solution_interview_revised.run,
    solution_dict.run
]

test_cases = [
    TestCase('Had had a funny haddock that had scales.', 'had'),
    TestCase('The two points pointed the pointing two', 'two'),
    TestCase('Yo yo', None),
    TestCase('Use "because" in first position rarely, and never more than one time in a piece of writing. In fact, vary your structures and the kinds of words you use throughout your writing.', 'in'),
    TestCase('This is a long sentence with lots and heaps of words, I do not:want,the Sentence to end just yet as this sentence is not long enough', 'sentence')

]

if __name__ == "__main__":
    results = run_tests(test_cases, solutions)
    for result in results:
        print "{0}\n".format(str(result))