import re

def get_word_iter(s):
    return (x.group(0) for x in re.finditer(r"[^\s\t\,\;\.\-]+", s))


def run(s):
    word_dict = {}
    for word in get_word_iter(s):
        if word in word_dict.keys():
            return word
        else:
            word_dict[word] = 1