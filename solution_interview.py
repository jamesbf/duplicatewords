# Complete the function below.
delimeters = [' ','\t',',',';','.','-']

class Node(object):
    def __init__( self, c, ep ):
        self.children=list()
        self.endPoint = ep
        self.char = c


def checkForWord(word, root_node):
    ''' Returns true if word has been found already'''
    currNode = root_node

    for index, c in enumerate(word):
        for child in currNode.children:
            if child.char == c:
                currNode = child
                break
        else:
            newNode = None
            for nc in word[index:]:
                newNode = Node(nc, False)
                currNode.children.append(newNode)
                currNode = newNode
            newNode.endPoint = True
            return False

    found = currNode.endPoint
    currNode.endPoint = True
    return found

def run(s):
    first_letter = -1
    root = Node( None, False )
    for index, c in enumerate(s):
        if c in delimeters:
            if first_letter != -1:
                word = s[first_letter:index]
                if checkForWord(word, root):
                    return word
                first_letter = -1
        else:
            if first_letter == -1:
                first_letter = index
    #Sentence ended with a word (alteration! missed in the interview)
    last_word = s[first_letter:]
    if first_letter != -1 and checkForWord(last_word, root):
        return last_word

